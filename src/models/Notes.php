<?php

class Notes
{
    private $title;
    private $description;

    public function __construct($title, $description)
    {
        $this->title = $title;
        $this->description = $description;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getDescription() :string
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

}