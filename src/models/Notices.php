<?php

class Notices
{
    private $price;
    private $time;
    private $notices_locality;
    private $title;
    private $description;
    private $days_hours;
    private $scope;

    public function __construct($price, $time, $notices_locality, $title, $description, $days_hours, $scope)
    {
        $this->price = $price;
        $this->time = $time;
        $this->notices_locality = $notices_locality;
        $this->title = $title;
        $this->description = $description;
        $this->days_hours = $days_hours;
        $this->scope = $scope;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time): void
    {
        $this->time = $time;
    }

    public function getNoticesLocality()
    {
        return $this->notices_locality;
    }

    public function setNoticesLocality($notices_locality): void
    {
        $this->notices_locality = $notices_locality;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getDaysHours()
    {
        return $this->days_hours;
    }

    public function setDaysHours($days_hours): void
    {
        $this->days_hours = $days_hours;
    }

    public function getScope()
    {
        return $this->scope;
    }

    public function setScope($scope): void
    {
        $this->scope = $scope;
    }


}