<?php

class Profile
{
    private $name;
    private $surname;
    private $phone_number;
    private $email_to_mess;
    private $locality;
    private $voivodeship;
    private $more_info;
    private $about_me;
    private $education;
    private $experience;


    public function __construct($name=null, $surname=null, $phone_number=null ,$email_to_mess=null, $locality=null, $voivodeship=null, $more_info=null, $about_me=null, $education=null, $experience=null)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->phone_number = $phone_number;
        $this->email_to_mess = $email_to_mess;
        $this->locality = $locality;
        $this->voivodeship = $voivodeship;
        $this->more_info = $more_info;
        $this->about_me = $about_me;
        $this->education = $education;
        $this->experience = $experience;
    }
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    public function setPhoneNumber($phone_number): void
    {
        $this->phone_number = $phone_number;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    public function getEmailToMess()
    {
        return $this->email_to_mess;
    }

    public function setEmailToMess($email_to_mess): void
    {
        $this->email_to_mess = $email_to_mess;
    }

    public function getLocality()
    {
        return $this->locality;
    }

    public function setLocality($locality): void
    {
        $this->locality = $locality;
    }

    public function getVoivodeship()
    {
        return $this->voivodeship;
    }

    public function setVoivodeship($voivodeship): void
    {
        $this->voivodeship = $voivodeship;
    }

    public function getMoreInfo()
    {
        return $this->more_info;
    }

    public function setMoreInfo($more_info): void
    {
        $this->more_info = $more_info;
    }

    public function getAboutMe()
    {
        return $this->about_me;
    }

    public function setAboutMe($about_me): void
    {
        $this->about_me = $about_me;
    }

    public function getEducation()
    {
        return $this->education;
    }

    public function setEducation($education): void
    {
        $this->education = $education;
    }

    public function getExperience()
    {
        return $this->experience;
    }

    public function setExperience($experience): void
    {
        $this->experience = $experience;
    }



}