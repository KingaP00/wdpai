<?php

require_once 'AppController.php';

class DefaultController extends AppController {

    public function index()
    {
        $this->render('start');
    }

    public function projects() // takie same nazwy jak te w url
    {
        $this->render('projects');
    }
    public function start() 
    {
        $this->render('start');
    }
    public function friends() 
    {
        $this->render('friends');
    }
    public function profile() 
    {
        $this->render('profile');
    }
    public function material() 
    {
        $this->render('material');
    }
    public function message() 
    {
        $this->render('message');
    }
    public function account() 
    {
        $this->render('account');
    }
    public function personaldata()
    {
        $this->render('personaldata');
    }
    public function areas() 
    {
        $this->render('areas');
    }
    public function settings() 
    {
        $this->render('settings');
    }
    public function notice() 
    {
        $this->render('notice');
    }
    public function noticegive() 
    {
        $this->render('noticegive');
    }
    public function noticesearch() 
    {
        $this->render('noticesearch');
    }
    public function noticecoach() 
    {
        $this->render('noticecoach');
    }
    public function noticestudent() 
    {
        $this->render('noticestudent');
    }
    public function noticelook() 
    {
        $this->render('noticelook');
    }
    public function notes()
    {
        $this->render('notes');
    }
    public function addnotes()
    {
        $this->render('addnotes');
    }
    public function addinfo()
    {
        $this->render('addinfo');
    }


}