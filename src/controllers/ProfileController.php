<?php

require_once 'AppController.php';
require_once __DIR__ .'/../models/Profile.php';
require_once __DIR__.'/../repository/ProfileRepository.php';

class ProfileController extends AppController
{
    private $messages = [];
    private $profileRepository;
    private $id;
    public function __construct()
    {
        parent::__construct();
        $this->profileRepository=new ProfileRepository();
        $this->id=$_COOKIE['id'];
    }

    public function addInfo(){

        if($this-> isPost()) {
            $profile = new Profile($_POST['name'], $_POST['surname'],$_POST['phone_number'],$_POST['email_to_mess'],
                $_POST['locality'],$_POST['voivodeship'],$_POST['more_info'],$_POST['about_me'],
                $_POST['education'],$_POST['experience'],$_FILES['file']['tmp_name'] );

       if($this->profileRepository->checkIfExist($this->id)){
           $this->profileRepository->updateInfo($profile, $this->id);


       }else{
           $this->profileRepository->addInfo($profile, $this->id);
       }

            return $this->render('profile', [
                'messages' => $this->messages,
                'profile' => $this->profileRepository->getInfo($this->id)]);
        }
        return $this->render('addinfo',['messages'=>$this->messages]);
    }
    public function profile(){
        if($this->profileRepository->checkIfExist($this->id)){
            $profile = $this->profileRepository->getInfo($this->id);
            $this->render('profile',['profile'=>$profile]);
        }else{
            $profile = new Profile();
            $this->render('profile',['profile'=>$profile]);
        }
    }



}

