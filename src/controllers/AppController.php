<?php

class AppController {
 private $request;

    public function __construct()
    {
        $this->request = $_SERVER['REQUEST_METHOD'];
    }

    protected function isPost():bool {
        return $this->request === 'POST';
    }

    protected function isGet():bool {
        return $this->request === 'GET';
    }

    protected function render(string $template = null, array $variables = []) //bedzie prxzyjmowala login lub projects
    {
        $templatePath = 'public/views/'. $template.'.php'; //tworzenie ścieżki( w windows public//views)
        $output = 'File not found';
                
        if(file_exists($templatePath)){ // czy pod tym adresem php znajduje sie ten plik
            extract($variables);


            //html otwiramy w strumieniu / okienku buforowania( kiedys potrzebne do odczytania zmiennych)
            ob_start(); // ptwiera strumien binarny
            include $templatePath;// załączymy plik html
            $output = ob_get_clean(); //
        }
        print $output; // drukujemy
    }
}