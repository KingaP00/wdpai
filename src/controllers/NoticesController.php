<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Notices.php';
require_once __DIR__.'/../repository/NoticesRepository.php';


class NoticesController extends AppController
{
    private $messages = [];
    private $noticesRepository;
    private $id;

    public function __construct()
    {
        parent::__construct();
        $this->noticesRepository=new NoticesRepository();
        $this->id=$_COOKIE['id'];
    }

    public function addNotices(){

        if($this-> isPost()) {
            $notices = new Notices($_POST['price'], $_POST['time'],$_POST['notices_locality'],
                $_POST['title'],$_POST['description'],$_POST['days_hours'],$_POST['scope']);
            $this->noticesRepository->addNotices($notices, $this->id);

            return $this->render('notices', [
                'messages' => $this->messages,
                'notices' => $this->noticesRepository->getNotices1($this->id)]);
        }
        return $this->render('noticegive',['messages'=>$this->messages]);
    }

    public function notices(){
        $notices = $this->noticesRepository->getNotices1($this->id);
        $this->render('notices',['notices'=>$notices]);
    }

    public function allnotices(){
        $notices = $this->noticesRepository->getNotices();
        $this->render('noticecoach',['notices'=>$notices]);
    }

}