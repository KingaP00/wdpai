<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Notes.php';
require_once __DIR__.'/../repository/NotesRepository.php';


class NotesController extends AppController
{
    private $messages = [];
    private $notesRepository;
    private $id;

    public function __construct()
    {
        parent::__construct();
        $this->notesRepository=new NotesRepository();
        $this->id=$_COOKIE['id'];
    }

    public function addNotes(){

        if($this-> isPost()) {
            $notes = new Notes($_POST['title'], $_POST['description']);
            $this->notesRepository->addNotes($notes, $this->id);

            return $this->render('notes', [
                'messages' => $this->messages,
                'notes' => $this->notesRepository->getNotes1($this->id)]);
        }
        return $this->render('addnotes',['messages'=>$this->messages]);
    }

    public function notes(){
        $notes = $this->notesRepository->getNotes1($this->id);
        $this->render('notes',['notes'=>$notes]);
    }
    public function search(){
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->notesRepository->getNotesByTitle($decoded['search']));
        }
    }
}