<?php
require_once "Repository.php";
require_once __DIR__.'/../models/Notices.php';
class NoticesRepository extends Repository
{
    public function getNotices(): array
    {
        $result = [];
        //połączenie z bazą danych
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.notices
        ');
        //wykonujemy stmt
        $stmt->execute();

        $notices = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($notices as $notice) {
            $result[]= new Notices(
                $notice['price'],
                $notice['time'],
                $notice['notices_locality'],
                $notice['title'],
                $notice['description'],
                $notice['days_hours'],
                $notice['scope']
            );
        }
        return $result;
    }

    public function getNotices1($id):array{
        $result = [];
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM notices WHERE notices.id_user_assigned_by = :id
        ');
        $stmt->bindParam(':id',$id,PDO::PARAM_INT);
        $stmt->execute();
        $notices = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($notices as $notice) {
            $result[]= new Notices(
                $notice['price'],
                $notice['time'],
                $notice['notices_locality'],
                $notice['title'],
                $notice['description'],
                $notice['days_hours'],
                $notice['scope']
            );
        }
        return $result;
}

    public function addNotices(Notices $notices , $id): void
    {
        $stmt = $this -> database->connect()->prepare('
        INSERT INTO notices(price,time,notices_locality,title,description,days_hours,scope,id_user_assigned_by)
        VALUES (?,?,?,?,?,?,?,?)
        ');

        $stmt->execute([
            $notices->getPrice(),
            $notices->getTime(),
            $notices->getNoticesLocality(),
            $notices->getTitle(),
            $notices->getDescription(),
            $notices->getDaysHours(),
            $notices->getScope(),
            $id
        ]);
    }

}