<?php
require_once "Repository.php";
require_once __DIR__.'/../models/Profile.php';
require_once __DIR__.'/../controllers/Debug.php';

class ProfileRepository extends Repository
{

    public function getInfo(int $id): ?Profile
    {
        //połączenie z bazą danych
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM profile WHERE profile.id_profile_created_by = :id
        ');

        //podłączamy parametry pod stmt
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        //wykonujemy stmt
        $stmt->execute();

        $profile = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($profile == false) {
            return null;
        }
        return new Profile(
            $profile['name'],
            $profile['surname'],
            $profile['phone_number'],
            $profile['email_to_mess'],
            $profile['locality'],
            $profile['voivodeship'],
            $profile['more_info'],
            $profile['about_me'],
            $profile['education'],
            $profile['experience']
        );
    }

    public function addInfo(Profile $profile , $id): void
    {
        $stmt = $this -> database->connect()->prepare('
        INSERT INTO profile(name, surname,phone_number,email_to_mess,
                            locality,voivodeship,more_info,about_me,
                            education,experience,id_profile_created_by)
        VALUES (?,?,?,?,?,?,?,?,?,?,?)
        ');

        $stmt->execute([
            $profile->getName(),
            $profile->getSurname(),
            $profile->getPhoneNumber(),
            $profile->getEmailToMess(),
            $profile->getLocality(),
            $profile->getVoivodeship(),
            $profile->getMoreInfo(),
            $profile->getAboutMe(),
            $profile->getEducation(),
            $profile->getExperience(),
            $id
        ]);
    }

    public function updateInfo(Profile $profile, $id): void
    {

        $stmt = $this->database->connect()->prepare('
                   UPDATE public.profile SET  name=:name , surname=:surname, 
                                             phone_number=:phone_number , about_me=:about_me,
                                             education=:education, experience=:experience,more_info=:more_info,
                                             email_to_mess=:email_to_mess,locality=:locality,voivodeship=:voivodeship
                                             WHERE profile.id_profile_created_by =:id                                                                             
        ');

        $name= $profile->getName();
        $surname = $profile->getSurname();
        $phone_number = $profile->getPhoneNumber();
        $about_me = $profile->getAboutMe();
        $education = $profile->getEducation();
        $experience = $profile->getExperience();
        $more_info= $profile->getMoreInfo();
        $email_to_mess = $profile->getEmailToMess();
        $locality =  $profile->getLocality();
        $voivodeship = $profile->getVoivodeship();
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stmt->bindParam(':phone_number', $phone_number, PDO::PARAM_STR);
        $stmt->bindParam(':about_me', $about_me, PDO::PARAM_STR);
        $stmt->bindParam(':education', $education, PDO::PARAM_STR);
        $stmt->bindParam(':experience', $experience, PDO::PARAM_STR);
        $stmt->bindParam(':more_info', $more_info, PDO::PARAM_STR);
        $stmt->bindParam(':email_to_mess', $email_to_mess, PDO::PARAM_STR);
        $stmt->bindParam(':locality', $locality, PDO::PARAM_STR);
        $stmt->bindParam(':voivodeship', $voivodeship, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function checkIfExist(int $id): bool{

        $profile =$this->getInfo($id);
        if(!empty($profile)){
            return true;
        }else {
            return false;
        }
    }

}