<?php
require_once "Repository.php";
require_once __DIR__.'/../models/Notes.php';
class NotesRepository extends Repository
{
    public function getNotes(int $id): ?Notes
    {
        //połączenie z bazą danych
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.notes WHERE id = :id
        ');

        //podłączmay parametry pod stmt
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        //wykonujemy stmt
        $stmt->execute();

        $notes = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($notes == false) {
            return null;
        }
        return new Notes(
            $notes['title_notes'],
            $notes['content']
        );
    }

    public function getNotes1($id):array{
        $result = [];
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM notes WHERE notes.id_user_created_by = :id
        ');
        $stmt->bindParam(':id',$id,PDO::PARAM_INT);
        $stmt->execute();
        $notes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($notes as $note) {
            $result[]= new Notes(
                $note['title_notes'],
                $note['content']
            );
        }
        return $result;
}

    public function addNotes(Notes $notes , $id): void
    {
        $date = new DateTime();
        $stmt = $this -> database->connect()->prepare('
        INSERT INTO notes(title_notes, content,created_at_notes, id_user_created_by)
        VALUES (?,?,?,?)
        ');


        $stmt->execute([
            $notes->getTitle(),
            $notes->getDescription(),
            $date->format('Y-m-d'),
            $id
        ]);
    }

    public function getNotesByTitle(string $searchString){
        $searchString = '%'.strtolower($searchString).'%'; // zamiana na małe litery
        $stmt =  $this->database->connect()->prepare('
        SELECT * FROM notes WHERE LOWER(title_notes) LIKE :search OR LOWER(content) LIKE :search 
        ');
        $stmt->bindParam(':search',$searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}