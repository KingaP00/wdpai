<?php

require_once __DIR__.'/../../Database.php';

class Repository
{
    protected $database;

    public function __construct()
    {
        //mamy obiekt bazy danych
        //Singleton-lub inne wzorce projektowe
        $this->database = new Database();
    }
}