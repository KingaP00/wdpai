<?php
require_once "Repository.php";
require_once __DIR__.'/../models/User.php';
class UserRepository extends Repository
{
    //funkcja, która znajdzie użytkownika po jego emailu;
    public function getUser(string $email): ?User
    {
        //połączenie z bazą danych
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users WHERE email = :email
        ');
        //podłączmay parametry pod stmt
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        //wykonujemy stmt
        $stmt->execute();
        //pobieramy uzytwonika z bazy za pomoca metody fetch
        //jako tabela asocjacyjna
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($user == false) {
            return null;
        } // jesli nie bedzie wpisu z uzytkownikikem o podanym mailu to stmt zwroci false

        return new User(
            $user['id_users'],
            $user['email'],
            $user['password'],
            $user['username']
        );
    }
    public function saveUser(User $user): String{
        if(($this->getUser($user->getEmail()))!=null){
            return "User already exist";
        }
        try{
            $stmt = $this->database->connect()->prepare('
                INSERT INTO public.users(email, password,username) VALUES (?,?,?);
            ');
            $stmt->execute([
                $user->getEmail(),
                $user->getPassword(),
                $user->getUsername()
            ]);
            return "User created";
        }
        catch(PDOException $e){
            return $e;
        }
    }


}