/*users*/
create table users
(
    id_users        serial
        constraint users_pk
            primary key,
    email           varchar(250) not null,
    password        varchar(250) not null,
    id_user_details integer
        constraint details_users___fk
            references profile
            on update cascade on delete cascade,
    id_grade        integer
        constraint users_grade___fk
            references grade_level
            on update cascade on delete cascade,
    username        varchar(50)  not null
);

alter table users
    owner to redvtsdamxuxlx;

create unique index users_id_users_uindex
    on users (id_users);

INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (5, 'jsnow@pk.edu.pl', 'admin', null, null, 'jsnow');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (6, 'ploszaj.kinga@gmail.com', 'admin1', null, null, 'KingaP00');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (7, 'test1@gmail.com', 'test', null, null, 'test1');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (8, 'testowy@gmail.com', 'admin', null, null, 'testowy');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (9, 'testowy3@gmail.com', 'test3', null, null, 'test3');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (10, 'te@gmail.com', 'te1', null, null, 'te');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (11, 'proba@gmail.com', 'proba', null, null, 'Proba');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (12, 'olx@gmail.com', 'olx', null, null, 'Olx');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (13, 'user1@gmail.com', 'user1', null, null, 'User1');
INSERT INTO public.users (id_users, email, password, id_user_details, id_grade, username) VALUES (14, 'probny@gmail.com', 'probny', null, null, 'Probny');

/*profile*/
create table profile
(
    id_user_details       integer default nextval('user_datails_id_user_details_seq'::regclass) not null
        constraint user_datails_pk
            primary key,
    name                  varchar(100)                                                          not null,
    surname               varchar(100)                                                          not null,
    phone_number          varchar(100),
    about_me              varchar,
    education             varchar,
    img_name              varchar,
    experience            varchar,
    more_info             varchar,
    email_to_mess         varchar,
    locality              varchar,
    voivodeship           varchar,
    id_profile_created_by integer                                                               not null
        constraint profile_users_id_users_fk
            references users
            on update cascade on delete cascade
);

alter table profile
    owner to redvtsdamxuxlx;

create unique index user_datails_id_user_details_uindex
    on profile (id_user_details);

INSERT INTO public.profile (id_user_details, name, surname, phone_number, about_me, education, img_name, experience, more_info, email_to_mess, locality, voivodeship, id_profile_created_by) VALUES (56, 'q', 'q', 'q', 'q', 'q', null, 'q', 'q', 'q', 'q', 'q', 13);
INSERT INTO public.profile (id_user_details, name, surname, phone_number, about_me, education, img_name, experience, more_info, email_to_mess, locality, voivodeship, id_profile_created_by) VALUES (53, 'a', 'a', 'a', 'a', 'a', null, 'a', 'a', 'a', 'a', 'a', 5);

/*notices*/
create table notices
(
    id_notices          serial
        constraint notices_pk
            primary key,
    price               varchar,
    time                varchar,
    notices_locality    varchar,
    title               varchar,
    description         varchar,
    days_hours          varchar,
    scope               varchar,
    id_user_assigned_by integer not null
        constraint notices_users___fk
            references users
            on update cascade on delete cascade
);

alter table notices
    owner to redvtsdamxuxlx;

create unique index notices_id_notices_uindex
    on notices (id_notices);

/*notices + users*/
create table users_notices
(
    id_user   integer not null
        constraint users_users_notices___fk
            references users
            on update cascade on delete cascade,
    id_notice integer not null
        constraint notices_users_notices___fk
            references notices
            on update cascade on delete cascade
);

INSERT INTO public.notices (id_notices, price, time, notices_locality, title, description, days_hours, scope, id_user_assigned_by) VALUES (53, '50', '60', 'sd', 'sds', 'sdds', 'sdsd', 'ssd', 6);
INSERT INTO public.notices (id_notices, price, time, notices_locality, title, description, days_hours, scope, id_user_assigned_by) VALUES (54, '30', '45', 'Online', 'Zrobie zadania', 'kkk', 'Sobota 10-13', 'Szkoła podstawowa', 5);
INSERT INTO public.notices (id_notices, price, time, notices_locality, title, description, days_hours, scope, id_user_assigned_by) VALUES (55, '100', '120', 'Krakow', 'Korepetycje grupowe', 'Udziele korepetycji', 'Poniedzialek 10-21', 'Szkoła średnia', 5);

alter table users_notices
    owner to redvtsdamxuxlx;

/*notes*/
create table notes
(
    id_notes           serial
        constraint notes_pk
            primary key,
    title_notes        varchar(50),
    content            varchar,
    created_at_notes   date,
    id_user_created_by integer not null
        constraint notes_user___fk
            references users
            on update cascade on delete cascade
);

alter table notes
    owner to redvtsdamxuxlx;

create unique index notes_id_notes_uindex
    on notes (id_notes);

INSERT INTO public.notes (id_notes, title_notes, content, created_at_notes, id_user_created_by) VALUES (31, 'pierwsza', 'notatka', '2022-01-31', 5);
INSERT INTO public.notes (id_notes, title_notes, content, created_at_notes, id_user_created_by) VALUES (32, 'druga notatka', '', '2022-01-31', 5);
INSERT INTO public.notes (id_notes, title_notes, content, created_at_notes, id_user_created_by) VALUES (33, 'probna', 'notatka', '2022-02-01', 12);
INSERT INTO public.notes (id_notes, title_notes, content, created_at_notes, id_user_created_by) VALUES (34, 'druga', 'notatka probna', '2022-02-01', 12);
INSERT INTO public.notes (id_notes, title_notes, content, created_at_notes, id_user_created_by) VALUES (35, 'trzecia', 'notatka', '2022-02-01', 12);
INSERT INTO public.notes (id_notes, title_notes, content, created_at_notes, id_user_created_by) VALUES (36, 'xxx', 'xd', '2022-02-01', 6);
INSERT INTO public.notes (id_notes, title_notes, content, created_at_notes, id_user_created_by) VALUES (37, 'xxx', 'yyy', '2022-02-01', 5);
INSERT INTO public.notes (id_notes, title_notes, content, created_at_notes, id_user_created_by) VALUES (38, 'trzeci', 'xxx', '2022-02-01', 6);

/*definitions*/
create table definitions
(
    id_definitions  serial
        constraint definitions_pk
            primary key,
    title_def       varchar(50) not null,
    description_def varchar,
    id_grade        integer     not null
        constraint definitions_grade___fk
            references grade_level
            on update cascade on delete cascade
);

alter table definitions
    owner to redvtsdamxuxlx;

create unique index definitions_id_definitions_uindex
    on definitions (id_definitions);

/*departments*/
create table departments
(
    id_department   serial
        constraint departments_pk
            primary key,
    name_department varchar,
    id_grade        integer not null
        constraint departments_grade___fk
            references grade_level
            on update cascade on delete cascade
);

alter table departments
    owner to redvtsdamxuxlx;

create unique index departments_id_department_uindex
    on departments (id_department);

/*grade level */
create table grade_level
(
    id_grade_level   serial
        constraint grade_level_pk
            primary key,
    name_grade_level varchar not null
);

alter table grade_level
    owner to redvtsdamxuxlx;

create unique index grade_level_id_grade_level_uindex
    on grade_level (id_grade_level);    

/*requirements*/
create table requirements
(
    id_requirements           serial
        constraint requirements_pk
            primary key,
    descritption_requirements varchar not null,
    id_grade                  integer not null
        constraint requirements_grade___fk
            references grade_level
            on update cascade on delete cascade
);

alter table requirements
    owner to redvtsdamxuxlx;

create unique index requirements_id_requirements_uindex
    on requirements (id_requirements);

/*requirements + definitions*/
create table requirements_definitions
(
    id_definition  integer not null
        constraint definitions___fk
            references definitions
            on update cascade on delete cascade,
    id_requirement integer not null
        constraint requirements___fk
            references requirements
            on update cascade on delete cascade
);

alter table requirements_definitions
    owner to redvtsdamxuxlx;
