#Math

###General Info Zadaniem aplikacji jest ułatwienie prowadzenia korepetycji z matematyki.

###Description Aplikacja dzieli się na kilka sekcji. Pierwszą z nich jest "mój profil", gdzie każdy użytkownik uzupełnia formularz podając informacje o swojej osobie. Kolejną sekcją są "ogłoszenia", gdzie uczniowie lub korepetytorzy mogą zamieścić ogłoszenie i znaleźć ogłoszenia innych użytkowników.W sekcji "notatki" możliwe jest dodawanie nowych oraz wyszukiwanie konretnych notatek.Dodatkowo za pomocą sekcji "wiadomości" użytkownicy są przekierowywani do messengera, gdzie mogą prowadzić konwersację.W przyszłości aplikacja zostanie uzupełniona o dwie kolejne sekcje "znajomi" oraz "materiały".
![img_6.png](screens/img_6.png)
![img_8.png](screens/img_8.png)
![img_7.png](screens/img_7.png)
![img.png](screens/img.png)
![img_1.png](screens/img_1.png)
![img_9.png](screens/img_9.png)
![img_2.png](screens/img_2.png)
![img_10.png](screens/img_10.png)
![img_3.png](screens/img_3.png)
![img_4.png](screens/img_4.png)
![img_5.png](screens/img_5.png)

