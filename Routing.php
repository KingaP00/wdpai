<?php

require_once 'src/controllers/DefaultController.php';
require_once 'src/controllers/SecurityController.php';
require_once 'src/controllers/ProfileController.php';
require_once 'src/controllers/NoticesController.php';
require_once 'src/controllers/NotesController.php';
class Routing{
    public static $routes;
    public static function get($url, $view) {
        self::$routes[$url] = $view;
      }
    public static function post($url, $view) {
        self::$routes[$url] = $view;
    }
    
      public static function run ($url) { // url - ściezka odczytana w index
        //meotdy statyczne uruchamiamy bezpośrednio z klasy
        $action = explode("/", $url)[0];
        if (!array_key_exists($action, self::$routes)) {
          die("Wrong url!");
        }
        //TODO call controller
    
        $controller = self::$routes[$action];
        $object = new $controller;
        $action = $action ?: 'index'; // skrocony operator ; przypisz sama siebie, jezeli jstes pusty to przydziel index
    
        $object->$action();
      }

}