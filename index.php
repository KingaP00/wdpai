<?php

/*$name = 'Kinga';
$count = 1;
$count++;

echo 'Zmeinna'.$count;

echo 'Hello there ,'.$name.'👋';
for ($i=10; $i>=1; $i--)
    echo $i;

*/




require 'Routing.php'; //importujemy; za każdym razem wczytuje plik
//require_once- wcxytany plik zostaje w pamieci

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Routing::get('register', 'SecurityController');
Routing::get('index', 'DefaultController');
Routing::get('projects', 'DefaultController');
Routing::get('start', 'DefaultController');
Routing::get('friends', 'DefaultController');
Routing::get('profile', 'ProfileController');
Routing::get('material', 'DefaultController');
Routing::get('account', 'DefaultController');
Routing::get('notice', 'DefaultController');
Routing::get('noticegive', 'DefaultController');
Routing::get('noticesearch', 'DefaultController');
Routing::get('allnotices', 'NoticesController');
Routing::get('noticestudent', 'DefaultController');
Routing::get('notices', 'NoticesController');
Routing::get('notes', 'NotesController');
Routing::get('addnotes', 'DefaultController');
Routing::get('addinfo', 'DefaultController');
Routing::post('login', 'SecurityController');
Routing::post('logout', 'SecurityController');
Routing::post('addNotes', 'NotesController');
Routing::get('updateInfo', 'ProfileController');
Routing::get('addInfo', 'ProfileController');
//Routing::post('addInfo', 'ProfileController');
Routing::post('search', 'NotesController');
Routing::post('addNotices', 'NoticesController');




Routing::run($path); //
