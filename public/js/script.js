const form = document.querySelector("form");
const emailInput = form.querySelector('input[name="email"]' );
const confirmedPasswordInput = form.querySelector('input[name="confirmedPassword"]');

//sprawdza czy podany email jest poprawny

function isEmail(email) {
    return /\S+@\S+\.\S+/.test(email);
}

//czy nasze hasła są takie same

function arePasswordsSame(password, confirmedPassword) {
    return password === confirmedPassword;
}

function markValidation(element, condition) {
    !condition ? element.classList.add('no-valid') : element.classList.remove('no-valid');
}
//opoznienie za pomocą setTimeout
function validateEmail() {
    setTimeout(function () {
            markValidation(emailInput, isEmail(emailInput.value));
        },
        1000
    );
}

function validatePassword() {
    setTimeout(function () {
            const condition = arePasswordsSame(
                confirmedPasswordInput.previousElementSibling.value,
                confirmedPasswordInput.value
            );
            markValidation(confirmedPasswordInput, condition);
        },
        1000
    );
}

//uruchomienie zdarzeń na inputach
emailInput.addEventListener('keyup', validateEmail);
confirmedPasswordInput.addEventListener('keyup', validatePassword);