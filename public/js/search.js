const search = document.querySelector('input[placeholder = "search"]');
const notesContainer = document.querySelector(".notes-search");


search.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault(); // zapobiegamy kolejnym akcją

        const data = {search: this.value}; //przypisujemy wartość wpisaną do search baru

        fetch("/search", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json' // nagłówek do protokołu HTTP
            },
            body: JSON.stringify(data) // konwertujemy na format JSON, aby przekazać na backend
        }).then(function (response) {
            return response.json();
        }).then(function (notes) {
            notesContainer.innerHTML = "";
            loadNotes(notes)
        });
    }
});

function loadNotes(notes) {
    notes.forEach(note => {
        console.log(note);
        createNote(note);
    });
}
function createNote(note) {
    const template = document.querySelector("#notes-template");

    const clone = template.content.cloneNode(true);

    const title_notes = clone.querySelector("h2");
    title_notes.innerHTML = note.title_notes;

    const content = clone.querySelector("p");
    content.innerHTML = note.content;

    notesContainer.appendChild(clone);
}