<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/notes.css">
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <script type ="text/javascript" src="./public/js/search.js" defer></script>
    <title>NOTES</title>
    <script src="https://kit.fontawesome.com/e016ffa5ce.js" crossorigin="anonymous"></script>
</head>
<body>
<div class="base-container">
    <nav>
        <img src="public/img/logo.svg">
        <ul>
            <li>
                <i class="far fa-user-circle"></i>
                <a href="http://localhost:8080/profile" class="button">Mój profil</a>
            </li>
            <li>
                <i class="fas fa-business-time"></i>
                <a href="http://localhost:8080/notice" class="button">Ogłoszenia</a>
            </li>
            <li>
                <i class="fas fa-user-friends"></i>
                <a href="http://localhost:8080/friends" class="button">Znajomi</a>
            </li>
            <li>
                <i class="fas fa-envelope"></i>
                <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F1513235615649685%2F%3Fref%3Dotworzoczy%26messaging_source%3Dsource%253Apages%253Amessage_shortlink" class="button">Wiadomości</a>
            </li>
            <li>
                <i class="fas fa-sticky-note"></i>
                <a href="http://localhost:8080/notes" class="button">Notatki</a>
            </li>
            <li>
                <i class="fas fa-book"></i>
                <a href="http://localhost:8080/material" class="button">Materiały</a>
            </li>
            <li>
                <form class="logout" action="logout" method ="POST">
                    <button type ="submit">WYLOGUJ</button>
                </form>
            </li>
        </ul>

    </nav>
    <main>
        <header>
            <div class= " serach-bar">
                    <input placeholder = "search">
                    <i class="far fa-user-circle fa-3x"></i>
            </div>
        </header>
        <div id = "title">
            <a href="http://localhost:8080/notes" class="button">Notatki</a>
        </div>
        <section class = "notes-search">
            <a href="http://localhost:8080/addnotes" class="button1">Dodaj notatkę</a>
            <?php foreach ($notes as $note): ?>
            <div id ="first">
                <div id="notatka">
                    <a>Tytuł</a>
                    <p id ="tytul"><?=$note->getTitle() ?></p>
                    <a>Notatka</a>
                    <p id ="data"><?=$note->getDescription() ?></p>
                </div>
            </div>
            <? endforeach; ?>
        </section>
    </main>
</div>
</body>

<template id ="notes-template">
    <div id ="first">
        <div id="notatka">
            <a>Tytuł</a>
            <h2 id ="tytul">title </h2>
            <a>Notatka</a>
            <p id ="data">description</p>
        </div>
    </div>
</template>

