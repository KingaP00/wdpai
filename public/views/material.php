<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="public/css/material.css">

    <title>MATERIAL</title>
    <script src="https://kit.fontawesome.com/e016ffa5ce.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="base-container">
        <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <i class="far fa-user-circle"></i>
                    <a href="http://localhost:8080/profile" class="button">Mój profil</a>
                </li>
                <li>
                    <i class="fas fa-business-time"></i>
                    <a href="http://localhost:8080/notice" class="button">Ogłoszenia</a>
                </li>
                <li>
                    <i class="fas fa-user-friends"></i>
                    <a href="http://localhost:8080/friends" class="button">Znajomi</a>
                </li>
                <li>
                    <i class="fas fa-envelope"></i>
                    <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F1513235615649685%2F%3Fref%3Dotworzoczy%26messaging_source%3Dsource%253Apages%253Amessage_shortlink" class="button">Wiadomości</a>
                </li>
                <li>
                    <i class="fas fa-sticky-note"></i>
                    <a href="http://localhost:8080/notes" class="button">Notatki</a>
                </li>
                <li>
                    <i class="fas fa-book"></i>
                    <a href="http://localhost:8080/material" class="button">Materiały</a>
                </li>
                <li>
                    <form class="logout" action="logout" method ="POST">
                        <button type ="submit">WYLOGUJ</button>
                    </form>
                </li>
            </ul>
            
        </nav>
        <main>
            <header>
                <div class= " serach-bar">
                    <input placeholder = "szukaj">
                    <i class="far fa-user-circle fa-3x"></i>
                </div>
            </header>
            <div id = "title">
                    <a href="http://localhost:8080/material" class="button">Materiały</a>
                </div>
            <section class = "material">
                <div class = "content1">
                    <div id ="class">
                        <div id= "title">
                            <img id = "zdj" src = "public/img/uploads/mathematics-gecdd199a2_1920.jpg">
                            <p>Klasa podstawowa I-III</p>
                        </div>
                        <div class = "def">
                            <a href="#" class="button">Definicje</a>
                            <a href="#" class="button">Wymagania</a>
                            <a href="#" class="button">Zadania</a>
                        </div>
                    </div>
                    <div id = "section">
                        <p id ="text0" >DZIAŁY</p>
                            <a id="text1" href="#" class="button">Dodawanie</a>
                            <img id ="img1" class= "math" src = "public/img/uploads/memphis-shapes-g73ea29610_1920 .png">
                            <a id  = "text2"href="#" class="button">Własności liczb</a>
                            <img id ="img2"class = "math" src = "public/img/uploads/numbers-g0596af40a_1920.jpg">
                            <a id="text3" href="#" class="button">Figury</a>
                            <img id= "img3" class= "math" src = "public/img/uploads/freehand-g235a64778_1280 (1).png">
                            <a id="text4" href="#" class="button">Inne</a>
                            <img id="img4"class= "math" src = "public/img/uploads/right-g53db5ae5e_1280 (1).png">
                        </div>
                </div>
                <div class = "content2">
                    <div id ="class">
                        <div id= "title">
                            <img id = "zdj" src = "public/img/uploads/mathematics-g7fa94432a_1280.jpg">
                            <p>Klasa podstawowa IV-VIII</p>
                        </div>
                        <div class = "def">
                            <a href="#" class="button">Definicje</a>
                            <a href="#" class="button">Wymagania</a>
                            <a href="#" class="button">Zadania</a>
                        </div>

                    </div>
                    <div id = "section">
                        <p id ="text0" >DZIAŁY</p>
                        <a id ="text1" href="#" class="button">Działania pisemne</a>
                        <img id ="img1" class= "math" src = "public/img/uploads/calculator-g303cf6e8b_1280 (1).png">
                        <a id ="text2" href="#" class="button">Potęgowanie</a>
                        <img id ="img2" class= "math" src = "public/img/uploads/mathematics-g7fa94432a_1280.jpg">
                        <a id ="text3" href="#" class="button">Geometria przestrzenna</a>
                        <img id ="img3" class= "math" src = "public/img/uploads/solids-gac2455aef_1280 (1).png">
                        <a id ="text4" href="#" class="button">Inne</a>
                        <img id ="img4" class= "math" src = "public/img/uploads/right-g53db5ae5e_1280 (1).png">
                    </div>
                </div>
                <div class = "content3">
                    <div id ="class">
                        <div id= "title">
                            <img id = "zdj" src = "public/img/uploads/geometry-gb86b82526_1920 (1).jpg">
                            <p>Liceum/Technikum</p>
                        </div>
                        <div class = "def">
                            <a href="#" class="button">Definicje</a>
                            <a href="#" class="button">Wymagania</a>
                            <a href="#" class="button">Zadania</a>
                        </div>

                    </div>
                    <div id = "section">
                        <p id ="text0" >DZIAŁY</p>
                            <a id ="text1" href="#" class="button">Równania</a>
                            <img id ="img1"  class= "math" src = "public/img/uploads/mathematics-g2b277326a_1920 (1).jpg">
                            <a  id ="text2" href="#" class="button">Figury obrotowe</a>
                            <img id ="img2" class= "math" src = "public/img/uploads/cone-g13ae953dc_1280 (1).png">
                            <a id ="text3" href="#" class="button">Funkcja kwadratowa</a>
                            <img  id ="img3" class= "math" src = "public/img/uploads/parabola-gd7fcceebb_1280 (1).png">
                            <a id ="text4" href="#" class="button">Inne</a>
                            <img id ="img4" class= "math" src = "public/img/uploads/right-g53db5ae5e_1280 (1).png">
                    </div>
                </div>
            </section>

        </main>
    </div>


</body>