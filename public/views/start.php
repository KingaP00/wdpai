<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/start.css">
    <title>START</title>
    <script src="https://kit.fontawesome.com/e016ffa5ce.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="login-container">
            <a href="http://localhost:8080/login" class="button">ZALOGUJ</a>
            <a href="http://localhost:8080/account" class="button1">UTWÓRZ KONTO</a>
        </div>
    </div>
</body>