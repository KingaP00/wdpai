<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="public/css/notices.css">
    <title>NOTICES/COACH</title>
    <script src="https://kit.fontawesome.com/e016ffa5ce.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="base-container">
        <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <i class="far fa-user-circle"></i>
                    <a href="http://localhost:8080/profile" class="button">Mój profil</a>
                </li>
                <li>
                    <i class="fas fa-business-time"></i>
                    <a href="http://localhost:8080/notice" class="button">Ogłoszenia</a>
                </li>
                <li>
                    <i class="fas fa-user-friends"></i>
                    <a href="http://localhost:8080/friends" class="button">Znajomi</a>
                </li>
                <li>
                    <i class="fas fa-envelope"></i>
                    <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F1513235615649685%2F%3Fref%3Dotworzoczy%26messaging_source%3Dsource%253Apages%253Amessage_shortlink" class="button">Wiadomości</a>
                </li>
                <li>
                    <i class="fas fa-sticky-note"></i>
                    <a href="http://localhost:8080/notes" class="button">Notatki</a>
                </li>
                <li>
                    <i class="fas fa-book"></i>
                    <a href="http://localhost:8080/material" class="button">Materiały</a>
                </li>
                <li>
                    <form class="logout" action="logout" method ="POST">
                        <button type ="submit">WYLOGUJ</button>
                    </form>
                </li>
            </ul>
            
        </nav>
        <main>
            <header>
                <div class= " serach-bar">
                    <input placeholder = "szukaj">
                    <i class="far fa-user-circle fa-3x"></i>
                </div>
            </header>
            <div id = "title">
                <a href="http://localhost:8080/noticecoach" class="button">Ogłoszenia</a>
            </div>
            <section class = "notice-search">
                <?php if (isset($notices))foreach ($notices as $notice): ?>
                <div id ="second">
                    <div id="ogloszenie">
                        <a id="tytul">Tytuł ogłoszenia</a>
                        <p id ="title"><?=$notice->getTitle(); ?></p>
                        <a id="tresc">Treść ogłoszenia</a>
                        <p id ="description"><?=$notice->getDescription(); ?></p>
                        <a>Cena</a>
                        <p id ="cena"><?=$notice->getPrice(); ?></p>
                        <a>Miejsca</a>
                        <p><?=$notice->getNoticesLocality(); ?></p>
                        <a>Dostępność</a>
                        <p><?=$notice->getDaysHours(); ?></p>
                        <a>Czas zajęć</a>
                        <p id ="czas"><?=$notice->getTime(); ?></p>
                        <a>Zakres zajęć</a>
                        <p><?=$notice->getScope(); ?></p>
                        <a href="#" class="button">Zobacz profil</a>
                    </div>
                </div>
                <?php endforeach; ?>
            </section>
        </main>
    </div>