<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="public/css/profile.css">
    <title>PROFILE/ME</title>
    <script src="https://kit.fontawesome.com/e016ffa5ce.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="base-container">
        <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <i class="far fa-user-circle"></i>
                    <a href="http://localhost:8080/profile" class="button">Mój profil</a>
                </li>
                <li>
                    <i class="fas fa-business-time"></i>
                    <a href="http://localhost:8080/notice" class="button">Ogłoszenia</a>
                </li>
                <li>
                    <i class="fas fa-user-friends"></i>
                    <a href="http://localhost:8080/friends" class="button">Znajomi</a>
                </li>
                <li>
                    <i class="fas fa-envelope"></i>
                    <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F1513235615649685%2F%3Fref%3Dotworzoczy%26messaging_source%3Dsource%253Apages%253Amessage_shortlink" class="button">Wiadomości</a>
                </li>
                <li>
                    <i class="fas fa-sticky-note"></i>
                    <a href="http://localhost:8080/notes" class="button">Notatki</a>
                </li>
                <li>
                    <i class="fas fa-book"></i>
                    <a href="http://localhost:8080/material" class="button">Materiały</a>
                </li>
                <li>
                    <form class="logout" action="logout" method ="POST">
                        <button type ="submit">WYLOGUJ</button>
                    </form>
                </li>
            </ul>
            
        </nav>
        <main>
            <header>
                <div class= " serach-bar">
                    <input placeholder = "szukaj">
                    <i class="far fa-user-circle fa-3x"></i>
                </div>
            </header>
            <div id = "title">
                <a href="http://localhost:8080/profile" class="button">Mój profil</a>
            </div>
            <section class = "profile">
                <img src = "public/img/uploads/zdj-projekt.jpg">
                <div id="info">
                    <a id="new">Moje dane</a>
                    <a id="new">Obszary działania</a>
                    <a id="new">O mnie</a>
                </div>
                <a href="http://localhost:8080/addinfo" class="button">Uzupełnij dane</a>
                <div id ="info1">
                    <p>Imię : </p>
                    <a><?= $profile->getName()?></a>
                    <p>Nazwisko : </p>
                    <a><?= $profile->getSurname()?></a>
                    <p>Numer telefonu : </p>
                    <a><?= $profile->getPhoneNumber()?></a>
                    <p>Adres e-mail : </p>
                    <a><?= $profile->getEmailToMess()?></a>
                    <p>O mnie : </p>
                    <a><?=$profile->getAboutMe() ?></a>
                    <p>Wykształcenie : </p>
                    <a><?=$profile->getEducation() ?></a>
                    <p>Doświadczenie : </p>
                    <a><?=$profile->getExperience() ?></a>
                    <p>Miejscowości : </p>
                    <a><?=$profile->getLocality() ?></a>
                    <p>Województwo : </p>
                    <a><?=$profile->getVoivodeship() ?></a>
                    <p>Dodatkowe informacje : </p>
                    <a><?=$profile->getMoreInfo() ?></a>

                </div>
            </section>
        </main>
    </div>


</body>