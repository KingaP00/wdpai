<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="public/css/noticelook.css">
    <title>NOTICES/LOOK</title>
    <script src="https://kit.fontawesome.com/e016ffa5ce.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="base-container">
        <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <i class="far fa-user-circle"></i>
                    <a href="http://localhost:8080/profile" class="button">Mój profil</a>
                </li>
                <li>
                    <i class="fas fa-business-time"></i>
                    <a href="http://localhost:8080/notice" class="button">Ogłoszenia</a>
                </li>
                <li>
                    <i class="fas fa-user-friends"></i>
                    <a href="http://localhost:8080/friends" class="button">Znajomi</a>
                </li>
                <li>
                    <i class="fas fa-envelope"></i>
                    <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F1513235615649685%2F%3Fref%3Dotworzoczy%26messaging_source%3Dsource%253Apages%253Amessage_shortlink" class="button">Wiadomości</a>
                </li>
                <li>
                    <i class="fas fa-sticky-note"></i>
                    <a href="http://localhost:8080/notes" class="button">Notatki</a>
                </li>
                <li>
                    <i class="fas fa-book"></i>
                    <a href="http://localhost:8080/material" class="button">Materiały</a>
                </li>
                <li>
                    <form class="logout" action="logout" method ="POST">
                        <button type ="submit">WYLOGUJ</button>
                    </form>
                </li>
            </ul>
            
        </nav>
        <main>
            <header>
                <div class= " serach-bar">
                    <input placeholder = "szukaj">
                    <i class="far fa-user-circle fa-3x"></i>
                </div>
            </header>
            <div id = "title">
                <a href="http://localhost:8080/noticelook" class="button">Ogłoszenia</a>
            </div>
            <section class = "notice-search">
                <div id ="first">
                    <img src = "public/img/uploads/woman-g8243b6c15_1920.jpg">
                    <a>Katarzyna Źródło</a>
                    <div id="miejsce">
                        <i class="fas fa-map-pin"></i>
                        <a>Miejsce</a>
                    </div>
                    <div id= "miejsca">
                        <p id="miejsce">Online</p>
                        <p id="miejsce">Kraków</p>
                    </div>
                    <div id="wiadomosc">
                        <i class="fas fa-envelope"></i>
                        <a href="#" class="button1">Napisz wiadomość</a>
                    </div>
                    <div id="telefon">
                        <i class="fas fa-phone"></i>
                        <a href="#" class="button1">Zadzwoń</a>
                    </div>
                </div>
                <?php foreach ($notices as $notice): ?>
                <div id ="second">
                    <a id="tytul">Tytuł ogłoszenia</a>
                    <p><?=$notice->getTitle() ?></p>
                    <a id="tresc">Treść ogłoszenia</a>
                    <p><?=$notice->getDescription() ?></p>
                    <a>Cena</a>
                    <p id ="cena"><?=$notice->getPrice() ?></p>
                    <a>Miejsca</a>
                    <p><?=$notice->getNoticesLocality() ?></p>
                    <a>Dostępność</a>
                    <p><?=$notice->getDaysHours() ?></p>
                    <a>Czas zajęć</a>
                    <p id ="czas"><?=$notice->getTime() ?></p>
                    <a>Zakres zajęć</a>
                    <p><?=$notice->getScope() ?></p>
                </div>
                <? endforeach; ?>
            </section>
        </main>
    </div>