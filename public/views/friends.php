<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <link rel="stylesheet" type="text/css" href="public/css/friends.css">

    <title>FRIENDS</title>
    <script src="https://kit.fontawesome.com/e016ffa5ce.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="base-container">
        <nav>
            <img src="public/img/logo.svg">
            <ul>
                <li>
                    <i class="far fa-user-circle"></i>
                    <a href="http://localhost:8080/profile" class="button">Mój profil</a>
                </li>
                <li>
                    <i class="fas fa-business-time"></i>
                    <a href="http://localhost:8080/notice" class="button">Ogłoszenia</a>
                </li>
                <li>
                    <i class="fas fa-user-friends"></i>
                    <a href="http://localhost:8080/friends" class="button">Znajomi</a>
                </li>
                <li>
                    <i class="fas fa-envelope"></i>
                    <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F1513235615649685%2F%3Fref%3Dotworzoczy%26messaging_source%3Dsource%253Apages%253Amessage_shortlink" class="button">Wiadomości</a>
                </li>
                <li>
                    <i class="fas fa-sticky-note"></i>
                    <a href="http://localhost:8080/notes" class="button">Notatki</a>
                </li>
                <li>
                    <i class="fas fa-book"></i>
                    <a href="http://localhost:8080/material" class="button">Materiały</a>
                </li>
                <li>
                    <form class="logout" action="logout" method ="POST">
                        <button type ="submit">WYLOGUJ</button>
                    </form>
                </li>
            </ul>
            
        </nav>
        <main>
            <header>
                <div class= " serach-bar">
                    <input placeholder = "szukaj">
                    <i class="far fa-user-circle fa-3x"></i>
                </div>
            </header>
            <div id = "title">
                    <a href="http://localhost:8080/friends" class="button">Znajomi</a>
            </div>
            <section class = "friends">
                <div id ="friend 1">
                    <img src = "public/img/uploads/boy-g22f274154_1920.jpg">
                    <div id ="person">
                        <h2>Piotr Lasek</h2>
                        <p>7 klasa podstawowa</p>
                        <div id ="message">
                            <i class="fas fa-comments "></i>
                            <i class="fas fa-phone  "></i>
                            <i class="fas fa-book "></i>
                        </div>
                    </div>
                </div>
                <div id ="friend 2" >
                    <img src = "public/img/uploads/girl-gb3e24417c_1920.jpg">
                    <div>
                        <h2>Kasia Nowak</h2>
                        <p>3 klasa podstawowa</p>
                        <div id ="message">
                            <i class="fas fa-comments "></i>
                            <i class="fas fa-phone  "></i>
                            <i class="fas fa-book"></i>
                        </div>
                    </div>
                </div>
                <div id ="friend 3" >
                    <img src = "public/img/uploads/child-g40bd3993d_1280.jpg">
                    <div>
                        <h2>Bartosz Papaj</h2>
                        <p>4 klasa podstawowa</p>
                        <div id ="message">
                            <i class="fas fa-comments "></i>
                            <i class="fas fa-phone  "></i>
                            <i class="fas fa-book "></i>
                        </div>
                    </div>
                </div>
                <div id ="friend 4" >
                    <img src = "public/img/uploads/boy-gd20c70d19_1920.jpg">
                    <div>
                        <h2>Kacper Grojec</h2>
                        <p>5 klasa podstawowa</p>
                        <div id ="message">
                            <i class="fas fa-comments "></i>
                            <i class="fas fa-phone "></i>
                            <i class="fas fa-book "></i>
                        </div>
                    </div>
                </div>
                <div id ="friend 5" >
                    <img src = "public/img/uploads/woman-g5ed4d2029_1920.jpg">
                    <div>
                        <h2>Zuzanna Maj</h2>
                        <p>2 klasa liceum</p>
                        <div id ="message">
                            <i class="fas fa-comments "></i>
                            <i class="fas fa-phone "></i>
                            <i class="fas fa-book  "></i>
                        </div>
                    </div>
                </div>
                <div id ="friend 6">
                    <img src = "public/img/uploads/woman-g0a5f95948_1920.jpg">
                    <div>
                        <h2>Alicja Gwóźdź</h2>
                        <p>1 klasa liceum</p>
                        <div id ="message">
                            <i class="fas fa-comments "></i>
                            <i class="fas fa-phone "></i>
                            <i class="fas fa-book "></i>
                        </div>
                    </div>
                </div>
            </section>

        </main>
    </div>


</body>