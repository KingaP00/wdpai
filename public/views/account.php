<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/login.css">
    <script type ="text/javascript" src="./public/js/script.js" defer></script>
    <title>ACCOUNT</title>
    <script src="https://kit.fontawesome.com/e016ffa5ce.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="login-container">
            <form class="login" action ="register" method ="POST">
                <input name="username" type="text" placeholder="nazwa użytkownika">
                <input name="email" type="text" placeholder="email@email.com">
                <input name="password" type="password" placeholder="hasło">
                <input name="confirmedPassword" type="password" placeholder="potwierdź hasło">
                <button id = "utworz" type ="submit">UTWÓRZ KONTO</button>
            </form>
        </div>
    </div>
</body>